import java.util.Scanner;

public class Game {
    public static void main(String[] args) {
        int numberToFind = (int)(Math.random()*11);
        Scanner scan = new Scanner(System.in);
        int tries = 0;
        int input;

        do {
            tries++;
            System.out.print("> ");
            input = scan.nextInt();

            if (input < numberToFind) {
                System.out.println("trop petit.");
            } else if (input > numberToFind) {
                System.out.println("trop grand.");
            } else {
                System.out.println("Gagné!");
                System.out.println("Nb essais: "+tries);
            }
        } while (input != numberToFind);
    }
}
